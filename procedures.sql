DELIMITER //
CREATE FUNCTION JourOuvre(jour date) 
   RETURNS date
BEGIN
  DECLARE i NUMERIC(8,2) DEFAULT 1;

  WHILE ((WEEKDAY(DATE_ADD(jour, INTERVAL i DAY)) = 5) || (WEEKDAY(DATE_ADD(jour, INTERVAL i DAY)) = 6)) DO
  	SET i = i + 1;
  END WHILE;
  RETURN DATE_ADD(jour, INTERVAL i DAY);
END;
//

DELIMITER //
CREATE PROCEDURE ChangerEtats()
BEGIN

 UPDATE t_commande 
 SET com_etat = 'P', com_retrait = DATE(NOW())
 WHERE com_etat = 'V'
 AND JourOuvre(com_date) = DATE(NOW());

 UPDATE t_commande 
 SET com_etat = 'E', com_retrait = DATE(NOW())
 WHERE com_etat = 'P'
 AND JourOuvre(com_retrait) = DATE(NOW());

 UPDATE t_commande 
 SET com_etat = 'D', com_retrait = DATE(NOW())
 WHERE com_etat = 'E'
 AND JourOuvre(com_retrait) = DATE(NOW());

 UPDATE t_commande 
 SET com_etat = 'X'
 WHERE com_etat = 'D'
 AND DATE(DATE_ADD(com_retrait, INTERVAL 1 MONTH)) <= DATE(NOW());

 UPDATE t_goodies
 JOIN tj_contenu USING (goo_id)
 JOIN t_commande USING (com_id)
 SET goo_stock = goo_stock + quantite
 WHERE com_etat = 'X'
 AND com_retrait != DATE(NOW());

 UPDATE t_commande
 SET com_retrait = DATE(NOW())
 WHERE com_etat = 'X';

END;
//