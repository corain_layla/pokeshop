<?php
	class Db_model extends CI_Model {

		public function __construct() {

			$this->load->database();
		}

		public function get_all_compte() {

			$query = $this->db->query("SELECT * FROM t_compte JOIN t_profil USING (pseudo);");
			return $query->result_array();
		}

		public function get_compte_check_creation() {

			$this->db->where('pseudo', htmlspecialchars(addslashes($this->input->post('id'))));

			$query = $this->db->get('t_compte');

			if($query->num_rows() > 0) {
				return false;
			} else {
				return true;
			}
		}

		public function get_total_compte() {

			$query = $this->db->query("SELECT count(*) as nb FROM t_compte;");
			return $query->row();
		}
		public function get_all_actualite() {

			$query = $this->db->query("SELECT * FROM t_actualite ORDER BY actu_date desc;");
			return $query->result_array();
		}

		public function set_actu() {
		
			$this->load->helper('url');

			$data = array(
				'actu_titre' => htmlspecialchars(addslashes($this->input->post('titre'))),
				'actu_contenu' => htmlspecialchars(addslashes($this->input->post('texte'))),
				'pseudo' => $_SESSION['username'],
			);

			return $this->db->insert('t_actualite', $data);
		}

		public function get_all_points_pseudo() {

			$query = $this->db->query("SELECT * FROM t_point_retrait JOIN t_compte USING (point_id);");
			return $query->result_array();
		}

		public function get_all_points() {

			$query = $this->db->query("SELECT * FROM t_point_retrait;");
			return $query->result_array();
		}

		public function get_all_commandes() {

			$query = $this->db->query(" SELECT * FROM t_commande 
										JOIN t_point_retrait USING (point_id) ORDER BY (point_nom);");
			return $query->result_array();
		}

		public function get_commandes_vendeur() {

			$query = $this->db->query(" SELECT * FROM t_commande 
										JOIN t_point_retrait USING (point_id)
										JOIN t_compte USING (point_id)
										WHERE pseudo = '".$_SESSION['username']."'
										AND com_etat != 'R'
										ORDER BY (com_date);");
			return $query->result_array();
		}

		public function set_compte() {
		
			$this->load->helper('url');

			$salt = 'HellIsEmptyAndAllTheDevilsAreThere!#69';
			$motdepasse = hash('sha256', htmlspecialchars(addslashes($this->input->post('mdp1'))).$salt);

			if (htmlspecialchars(addslashes($this->input->post('statut'))) == 'V') {
				
				$data = array(
					'pseudo' => htmlspecialchars(addslashes($this->input->post('id'))),
					'password' => $motdepasse,
					'statut' => htmlspecialchars(addslashes($this->input->post('statut'))),
					'point_id' => htmlspecialchars(addslashes($this->input->post('point'))),
				);

			} else {
				$data = array(
					'pseudo' => htmlspecialchars(addslashes($this->input->post('id'))),
					'password' => $motdepasse,
					'statut' => htmlspecialchars(addslashes($this->input->post('statut'))),
				);
			}


			return $this->db->insert('t_compte', $data);
		}

		public function set_profil() {
		
			$this->load->helper('url');

			$data = array(
				'pseudo' => htmlspecialchars(addslashes($this->input->post('id'))),
				'pro_nom' => htmlspecialchars(addslashes($this->input->post('nom'))),
				'pro_prenom' => htmlspecialchars(addslashes($this->input->post('prenom'))),
				'pro_mail' => htmlspecialchars(addslashes($this->input->post('mail'))),
			);

			return $this->db->insert('t_profil', $data);
		}

		public function connect_compte($username, $password) {

			$salt = 'HellIsEmptyAndAllTheDevilsAreThere!#69';

			$this->db->where('pseudo', $username);
			$this->db->where('password', hash('sha256', $password.$salt));

			$query = $this->db->get('t_compte');

			if($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}

		public function get_statut_connexion($username) {

			/*$query = $this->db->query(" SELECT statut FROM t_compte
										WHERE pseudo='".$username."';");*/

			$this->db->where('pseudo', $username);
			$query = $this->db->get('t_compte');

			return $query->row();
		}

		public function get_profil($id) {

			$query = $this->db->query(" SELECT * FROM t_profil
										WHERE pseudo='".$id."';");
			return $query->row();
		}

		public function update_profil_compte() {

			$data = array(
			        'pro_nom' => htmlspecialchars(addslashes($this->input->post('nom'))),
			        'pro_prenom' => htmlspecialchars(addslashes($this->input->post('prenom'))),
			        'pro_mail' => htmlspecialchars(addslashes($this->input->post('mail'))),
			);

			$this->db->where('pseudo', $_SESSION['username']);
			$this->db->update('t_profil', $data);

			$salt = 'HellIsEmptyAndAllTheDevilsAreThere!#69';
			$motdepasse = hash('sha256', htmlspecialchars(addslashes($this->input->post('mdp1')).$salt));

			$data = array( 
					'password' => $motdepasse,
			);

			$this->db->where('pseudo', $_SESSION['username']);
			$this->db->update('t_compte', $data);
		}

		public function generate_recap($commande, $client) {

			$this->db->where('com_id', $commande);
			$this->db->where('code_client', $client);

			$query = $this->db->get('t_commande');

			if($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}

		public function get_commande($id) {

			$query = $this->db->query(" SELECT * FROM t_commande 
										JOIN t_point_retrait USING (point_id) 
										WHERE com_id='".$id."';");

			if($query->num_rows() > 0) {
				return $query->row();
			} else {
				return false;
			}
		}

		public function get_goodies_commande($id) {

			$query = $this->db->query(" SELECT * FROM tj_contenu
										JOIN t_goodies USING (goo_id) 
										WHERE com_id='".$id."';");


			return $query->result_array();

		}

		public function update_stock_del($commande) {

			$query = $this->db->query(" UPDATE t_goodies
										JOIN tj_contenu USING (goo_id)
										SET goo_stock = goo_stock + quantite
										WHERE com_id ='".addslashes($commande)."';");

		}

		public function update_stock_com($commande) {

			$query = $this->db->query(" UPDATE t_goodies
										JOIN tj_contenu USING (goo_id)
										SET goo_stock = goo_stock - quantite
										WHERE com_id ='".addslashes($commande)."';");
		}

		public function delete_commande($commande) {

			$this->db->delete('tj_contenu', array('com_id' => addslashes($commande)));
			$this->db->delete('t_commande', array('com_id' => addslashes($commande)));
		}

		public function get_all_originaux () {

			$query = $this->db->query(" SELECT * FROM t_original
										JOIN t_categorie USING (cat_id) ORDER BY (cat_id);");
			return $query->result_array();

		}

		public function get_all_categories() {

			$query = $this->db->query(" SELECT * FROM t_categorie;");
			return $query->result_array();
		}

		public function get_originaux_cat($id) {

			$query = $this->db->query(" SELECT * FROM t_original JOIN t_categorie USING (cat_id) WHERE cat_id =".$id." ORDER BY (cat_id);");
			return $query->result_array();
		}

		public function get_originaux($id) {

			$query = $this->db->query(" SELECT * FROM t_original JOIN t_categorie USING (cat_id) WHERE ori_id =".$id.";");
			return $query->row();
		}

		public function get_goodies_original($id) {

			$query = $this->db->query(" SELECT * FROM t_original 
										JOIN tj_liste USING (ori_id)
										JOIN t_goodies USING (goo_id)
										WHERE ori_id =".$id."
										ORDER BY (goo_prix);");
			return $query->result_array();
		}

		public function get_all_type_goodies () {

			$query = $this->db->query(" SELECT * FROM t_type_goodie;");
			return $query->result_array();

		}

		public function get_all_goodies() {

			$query = $this->db->query(" SELECT * FROM t_goodies ORDER BY (goo_prix);");
			return $query->result_array();
		}

		public function get_goodies_type($type) {

			$query = $this->db->query(" SELECT * FROM t_goodies
										WHERE type_id =".$type." ORDER BY (goo_prix);");
			return $query->result_array();
		}

		public function get_goodie($id) {

			$query = $this->db->query(" SELECT * FROM t_goodies
										WHERE goo_id =".$id.";");
			return $query->row();
		}

		public function get_stock($id) {

			$query = $this->db->query(" SELECT goo_stock FROM t_goodies
										WHERE goo_id =".$id.";");
			return $query->row();
		}

		public function update_etat_commande($id, $etat) {

			$this->load->helper('date');

			$datestring = '%Y-%m-%d %H:%i:%s';
			$time = NOW();

			$data = array( 
					'com_etat' => $etat,
					'com_retrait' => mdate($datestring, $time),
			);

			$this->db->where('com_id', $id);
			$this->db->update('t_commande', $data);

		}

		public function set_commande($MD5, $prix) {
		
			$this->load->helper('url');
			$this->load->helper('date');

			$datestring = '%Y-%m-%d %H:%i:%s';
			$datecom = NOW();
				
			$data = array(
				'com_id' => substr($MD5, 0, 20),
				'com_date' => mdate($datestring, $datecom),
				'com_etat' => 'V',
				'com_prix' => $prix,
				'com_nom' => htmlspecialchars(addslashes($this->input->post('nom'))),
				'com_prenom' => htmlspecialchars(addslashes($this->input->post('prenom'))),
				'com_mail' => htmlspecialchars(addslashes($this->input->post('mail'))),
				'code_client' => substr(md5(htmlspecialchars(addslashes($this->input->post('mail')))), 20, 8),
				'point_id' => htmlspecialchars(addslashes($this->input->post('point'))),
			);

			return $this->db->insert('t_commande', $data);
		}

		public function set_contenu($MD5, $id, $qty) {
		
			$this->load->helper('url');
				
			$data = array(
				'com_id' => substr($MD5, 0, 20),
				'goo_id' => $id,
				'quantite' => $qty,
			);

			return $this->db->insert('tj_contenu', $data);
		}

		public function check_mail($md5) {

			$this->db->where('com_id', substr($md5, 0, 20));

			$query = $this->db->get('t_commande');

			if($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}
	}
?>