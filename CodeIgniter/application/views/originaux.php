          <div class="col-lg-9">

            <div class="card card-outline-secondary my-4">
              <div class="card-header">
                Recherche par catégorie
              </div>
              <div class="card-body">

                <?php
                  echo validation_errors();
                  echo form_open('recherche_categorie');?>
                    <div class="form-group">
                      <label for="cat">Sélectionner un original:</label>
                      <select class="form-control" name="cat">
                        <option value="0">Aucun</option>
                        <?php 

                          foreach ($categories as $cat) {
                            echo"<option value=\"".$cat['cat_id']."\">".$cat['cat_nom']."</option>";
                          }
                        
                        ?>
                    </div>

                    <input type="submit" name="submit" value="Rechercher" class="btn btn-success mt-4" />
                  </form>

              </div>
            </div>

          </div>

            <div class="row">

              <?php 

                foreach ($originaux as $ori) {

                  echo "<div class=\"col-lg-4 col-md-6 mb-4\">
                    <div class=\"card h-100\">
                      <a href=\"".$this->config->item('base_url')."/index.php/goodiesOriginal/afficher/".$ori['ori_id']."\"><img class=\"card-img-top\" src=\"".$this->config->item('base_url')."/images/".$ori['ori_image']."\" alt=\"\"></a>
                      <div class=\"card-body\">
                        <h4 class=\"card-title\">
                          <a href=\"".$this->config->item('base_url')."/index.php/goodiesOriginal/afficher/".$ori['ori_id']."\">".$ori['ori_nom']."</a>
                        </h4>
                        <!--<p class=\"card-text\">".$ori['ori_description']."</p>-->
                      </div>
                      <div class=\"card-footer\">
                        <small class=\"text-muted\">Catégorie : ".$ori['cat_nom']."</small>
                      </div>
                    </div>
                  </div>";

                }

              ?>

            </div>

