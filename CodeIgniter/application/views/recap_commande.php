      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            Générer mon récapitulatif de commande
          </div>
          <div class="card-body">

            <p>Vous êtes client et vous avez passé commande? Insérez votre code commande et votre code client et générez un récapitulatif de votre commande.</p>
            
            <?php 
              echo $erreur;
              
              echo validation_errors();
              echo form_open('recap_commande'); ?>
                <div class="form-group">
                  <label for="com">Code Commande</label>
                  <input type="input" name="com" maxlength="20" required="required" pattern="[0-9a-z]+" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="cli">Code Client</label>
                  <input type="input" name="cli" required="required" pattern="[0-9a-z]+" class="form-control"/><br />
                </div>

                <input type="submit" name="submit" value="Générer" class="btn btn-success" />
              </form>

          </div>
        </div>
        
      </div>