<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shop Homepage</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo $this->config->base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo $this->config->base_url(); ?>css/shop-homepage.css" rel="stylesheet">
  <link href="<?php echo $this->config->base_url(); ?>css/shop-item.css" rel="stylesheet">

  <style>

    .logo{
      width: 30%;
      height: auto;
    }

    .img-custom {
      width: 100%;
      height: auto;
      display: block;
    }
    
    body {
      <?php echo "background-image: url(\"".$this->config->base_url()."/images/background.png\");"; ?>
      background-repeat: repeat;
    }

  </style>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <?php echo "<a class=\"navbar-brand\" href=\"#\"><img class=\"logo\" src=\"".$this->config->item('base_url')."/images/logo.png\"></a>";?>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <?php echo"<a class=\"nav-link\" href=\"".$this->config->item('base_url')."\">Accueil</a>"; ?>
              <span class="sr-only"></span>
            </a>
          </li>
          <li class="nav-item">
            <?php echo "<a class=\"nav-link\" href=\"".$this->config->item('base_url')."/index.php/panier/afficher\">Panier (".$this->cart->total_items().")</a>"; ?>
          </li>
          
          <?php 

            if (isset($_SESSION['username'])) {
              echo "<li class=\"nav-item\">
                <a class=\"nav-link\" href=\"".$this->config->item('base_url')."/index.php/accueilGestionnaire/afficher/0\">Mon profil (".stripslashes($_SESSION['username']).")</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"".$this->config->item('base_url')."/index.php/deconnexion/logout\">Déconnection</a>
              </li>";

            } else {

              echo "<li class=\"nav-item\">
                <a class=\"nav-link\" href=\"".$this->config->item('base_url')."/index.php/connexion/afficher/0\">Connexion</a>
              </li>";
            }
            
          ?>

        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
