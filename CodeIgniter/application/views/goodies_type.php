          <div class="col-lg-9">

            <div class="card card-outline-secondary my-4">
              <div class="card-header">
                Recherche par type
              </div>
              <div class="card-body">

                <?php
                  echo validation_errors();
                  echo form_open('recherche_type');?>
                    <div class="form-group">
                      <label for="type">Sélectionner un type:</label>
                      <select class="form-control" name="type">
                        <option value="0">Aucun</option>
                        <?php 

                          foreach ($types as $type) {
                            echo"<option value=\"".$type['type_id']."\">".$type['type_nom']."</option>";
                          }
                        
                        ?>
                    </div>

                    <input type="submit" name="submit" value="Rechercher" class="btn btn-success mt-4" />
                  </form>

              </div>
            </div>

          </div>

            <div class="row">

              <?php 

                foreach ($goodies as $goo) {

                  echo "<div class=\"col-lg-4 col-md-6 mb-4\">
                    <div class=\"card h-100\">
                      <a href=\"".$this->config->item('base_url')."/index.php/afficherGoodie/afficher/".$goo['goo_id']."\"><img class=\"card-img-top\" src=\"".$this->config->item('base_url')."/images/".$goo['goo_image']."\" alt=\"\"></a>
                      <div class=\"card-body\">
                        <h4 class=\"card-title\">
                          <a href=\"".$this->config->item('base_url')."/index.php/afficherGoodie/afficher/".$goo['goo_id']."\">".$goo['goo_nom']."</a>
                        </h4>
                        <!--<p class=\"card-text\">".$goo['goo_description']."</p>-->
                      </div>
                      <div class=\"card-footer\">
                        <small class=\"text-muted\">Prix : ".$goo['goo_prix']." €</small>
                      </div>
                    </div>
                  </div>";

                }

              ?>

            </div>