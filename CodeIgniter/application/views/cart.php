<div class="col-lg-9">

        <div class="card card-outline-secondary my-4">

                <div class="card-header">
                Mon panier
                </div>
                <div class="card-body">

                        <?php echo form_open('update_panier'); ?>

                        <table cellpadding="6" cellspacing="1" style="width:100%" border="0">

                        <tr>
                                <th></th>
                                <th>Quantité</th>
                                <th>Nom</th>
                                <th style="text-align:right">Prix</th>
                                <th style="text-align:right">Total</th>
                                
                        </tr>

                        <?php $i = 0; ?>

                        <?php foreach ($this->cart->contents() as $items): ?>

                                <?php   echo form_hidden($i.'[rowid]', $items['rowid']); 
                                        echo "<input type=hidden name=".$i."[id] value=".$items['id'].">"; ?>

                                <tr>
                                        <?php echo "<td><a href=\"".$this->config->item('base_url')."/index.php/panier/DeleteItem/".$items['rowid']."\"><small>Supprimer</small></a></td>"; ?>
                                        <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5', 'pattern' => '[0-9]+')); ?></td>
                                        <td>
                                                <?php echo $items['name']; ?>

                                        </td>
                                        <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?>€</td>
                                        <td style="text-align:right"><?php echo $this->cart->format_number($items['subtotal']); ?>€</td>
                                </tr>

                        <?php $i++; ?>
                        <?php endforeach; ?>

                        <?php   

                        if ($i == 0) {

                                echo "<p><strong>Votre panier est vide!</strong></p>";

                        } else {

                                echo form_hidden('n', $i);

                                echo"<tr>
                                        <td colspan=\"2\"> </td>
                                        <td class=\"right\"><strong>Total</strong></td>
                                        <td class=\"right\">".$this->cart->format_number($this->cart->total())."€</td>
                                </tr>

                                </table>

                                <input type=\"submit\" name=\"submit\" value=\"Mettre à jour le panier\" class=\"btn btn-primary\" />

                                <a href=\"".$this->config->item('base_url')."/index.php/confirmerCommande/afficher\"><button type=\"button\" class=\"btn btn-success\">Valider ma commande</button></a>";

                        }

                        ?>

                        

                </div>

        </div>

</div>