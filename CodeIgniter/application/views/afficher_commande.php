      <div class="col-lg-9">

        <?php

            if ($result->com_etat == 'V') {

              $etat = 'Validé';

            } else if ($result->com_etat == 'P') {

              $etat = 'Préparé';

            } else if ($result->com_etat == 'E') {

              $etat = 'Expédié';

            } else if ($result->com_etat == 'D') {

              $etat = 'Disponible';

            } else if ($result->com_etat == 'R') {

              $etat = 'Retiré';

            } else if ($result->com_etat == 'A') {

              $etat = 'Abandonné';

            } else if ($result->com_etat == 'X') {

              $etat = 'Expiré';
            }

            echo "<div class=\"card card-outline-secondary my-4\">

              <div class=\"card-header\">
                Commande ".$result->com_id."
              </div>
              <div class=\"card-body\">

                <table class=\"table\">
                  <thead>
                    <tr>
                      <th>Identité client</th>
                      <th>E-mail</th>
                      <th>Code client</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>".$result->com_prenom." ".$result->com_nom."</td>
                      <td>".$result->com_mail."</td>
                      <td>".$result->code_client."</td>
                    </tr>
                  </tbody>
                </table>

                <table class=\"table\">
                  <thead>
                    <tr>
                      <th>Point de retrait</th>
                      <th>Adresse</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>".$result->point_nom."</td>
                      <td>".$result->point_adresse."</td>
                    </tr>
                  </tbody>
                </table>

                <table class=\"table\">
                  <thead>
                    <tr>
                      <th>Date de commande</th>
                      <th>Dernier changement d'état</th>
                      <th>Etat actuel</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>".$result->com_date."</td>
                      <td>".$result->com_retrait."</td>
                      <td>".$etat."</td>
                    </tr>
                  </tbody>
                </table>

                <table class=\"table\">
                  <thead>
                    <tr>
                      <th>Objet</th>
                      <th>Quantité</th>
                      <th>Prix (unité)</th>
                    </tr>
                  </thead>
                  <tbody>";

                foreach ($result1 as $goodie) {

                  echo "<tr>
                          <td>".$goodie['goo_nom']."</td>
                          <td>".$goodie['quantite']."</td>
                          <td>".$goodie['goo_prix']." €</td>
                        </tr>";
                }

                echo "</thead>
                    <tbody>
                  </table>
                  <strong>TOTAL = ".$result->com_prix."€</strong>";

                if ((!isset($_SESSION['username'])) && ($result->com_etat != 'A') && ($result->com_etat != 'X')) {

                  if ($msgbottom == '') {
                    echo "<hr/>
                    <a href=\"".$this->config->item('base_url')."/index.php/recapCommande/recap/1\"><button type=\"button\" class=\"btn btn-success\">Supprimer ma commande</button></a>";
                  } else {

                    echo $msgbottom;

                  }

                }
        ?>
              </div>
            </div>

      </div>