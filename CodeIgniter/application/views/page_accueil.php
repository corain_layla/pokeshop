      <div class="col-lg-9">

        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <?php echo"<img class=\"d-block img-fluid\" src=\"".$this->config->item('base_url')."/images/slider/slide.png\" alt=\"First slide\">
            </div>
            <div class=\"carousel-item\">
              <img class=\"d-block img-fluid\" src=\"".$this->config->item('base_url')."/images/slider/slide2.png\" alt=\"Second slide\">
            </div>
            <div class=\"carousel-item\">
              <img class=\"d-block img-fluid\" src=\"".$this->config->item('base_url')."/images/slider/slide3.png\" alt=\"Third slide\">"; ?>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            Actualités
          </div>
          <div class="card-body">

            <?php

              foreach($actus as $login) {

                echo "<p><strong>".parse_smileys(stripslashes($login["actu_titre"]), $this->config->item('base_url').'/images/smileys')."</strong></p><p>".parse_smileys(stripslashes($login["actu_contenu"]), $this->config->item('base_url').'/images/smileys')."</p>";
                echo "<small class=\"text-muted\">Posté par ".$login["pseudo"]." le ".$login["actu_date"]."</small><hr>";
              }
            ?>
          </div>
        </div>

      </div>
      <!-- /.col-lg-9 -->