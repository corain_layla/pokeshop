      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
          <div class="card-body">

          <?php


              if ($_SESSION['statut'] == 'V') {
                echo $msgtop;
              }

              echo "<br/><table class=\"table table-hover\">

              <thead>
                <tr>
                  <th>Point de retrait</th>
                  <th>Numéro</th>
                  <th>Date commande</th>
                  <th>Dernier changement d'état</th>
                  <th>Etat actuel</th>
                  <th></th>
                </tr>
              </thead>

              <tbody>";

              foreach ($commande as $com) {

                echo "<tr>
                  <td>".$com["point_nom"]."</td>
                  <td>".$com["com_id"]."</td>
                  <td>".$com["com_date"]."</td>
                  <td>".$com["com_retrait"]."</td>
                  <td>".$com["com_etat"]."</td>
                  <td><a href=\"".$this->config->item('base_url')."/index.php/commandesListe/afficher/".$com["com_id"]."\">Voir plus</a></td>
                </tr>";
              }

              echo "</tbody>
              </table>";

              if ($_SESSION['statut'] == 'V') {

                if ($msgbottom == '') {

                echo form_open('modifier_etat');
                echo "<div class=\"form-group\">
                    <label for=\"id\">Commande</label>
                    <select class=\"form-control\" name=\"id\">";

                      foreach ($commande as $com) {
                        echo "<option value=\"".$com['com_id']."\">".$com['com_id']."</option>";
                      }
                      
                    echo "</select>
                  </div>
                  <div class=\"form-group\">
                    <label for=\"etat\">Modifier état</label>
                    <select class=\"form-control\" name=\"etat\">
                      <option value=\"R\">Retiré</option>
                      <option value=\"A\">Annulé</option>
                    </select>
                  </div>
                  <input type=\"submit\" name=\"submit\" value=\"Mettre à jour\" class=\"btn btn-success\">
                </form>"; 

              } else {

                echo $msgbottom;
              }


              }

              ?>
            </div>
          </div>
           
      </div>