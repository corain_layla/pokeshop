        <div class="col-lg-9">
          <div class="card card-outline-secondary my-4">
            <div class="card-header">
              Actualités
            </div>
            <div class="card-body">

              <?php

                echo smiley_js();
                
                foreach($result as $login) {

                  echo "<p><strong>".parse_smileys(stripslashes($login["actu_titre"]), $this->config->item('base_url').'/images/smileys')."</strong></p><p>".parse_smileys(stripslashes($login["actu_contenu"]), $this->config->item('base_url').'/images/smileys')."</p>";
                  echo "<small class=\"text-muted\">Posté par ".stripslashes($login["pseudo"])." le ".$login["actu_date"]."</small><hr>";
                }
              
               echo "<a href=\"".$this->config->item('base_url')."/index.php/actusListe/ajouter/0\" class=\"btn btn-success\">Ajouter une actualité</a>";
              ?>
            </div>
          </div>
        </div>