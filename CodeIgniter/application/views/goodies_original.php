          <div class="col-lg-9">

            <div class="card card-outline-secondary my-4">
              <div class="card-header">
                <?php echo"Original <strong>".$original->ori_nom."</strong>
              </div>
              <div class=\"card-body\">

                ".$original->ori_description;
                echo "<img src=\"".$this->config->item('base_url')."/images/".$original->ori_image."\" class=\"img-fluid rounded mx-auto d-block mt-4 mb-1\"/><p>Créé par : ".$original->pseudo."</p>";?>

              </div>
            </div>

            <div class="row">

              <?php 

                foreach ($result as $row) {

                  echo "<div class=\"col-lg-4 col-md-6 mb-4\">
                    <div class=\"card h-100\">
                      <a href=\"".$this->config->item('base_url')."/index.php/afficherGoodie/afficher/".$row['goo_id']."\"><img class=\"card-img-top\" src=\"".$this->config->item('base_url')."/images/".$row['goo_image']."\" alt=\"\"></a>
                      <div class=\"card-body\">
                        <h4 class=\"card-title\">
                          <a href=\"".$this->config->item('base_url')."/index.php/afficherGoodie/afficher/".$row['goo_id']."\">".$row['goo_nom']."</a>
                        </h4>
                      </div>
                      <div class=\"card-footer\">
                        <p class=\"text-muted\">Prix : ".$row['goo_prix']." €</p>
                      </div>
                    </div>
                  </div>";

                }

              ?>
            </div>

          </div>