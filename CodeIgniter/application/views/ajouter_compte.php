      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
            Créer un compte
            </div>
            <div class="card-body">
              <?php 

                echo $erreur;

                echo validation_errors();
                echo form_open('ajouter_compte'); ?>
                <div class="form-group">
                  <label for="id">Identifiant</label>
                  <input type="input" name="id" maxlength="16" required="required" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="nom">Nom</label>
                  <input type="input" name="nom" maxlength="45" required="required" pattern="[A-Za-zÀ-ÿ '-]+" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="prenom">Prénom</label>
                  <input type="input" name="prenom" maxlength="45" required="required" pattern="[A-Za-zÀ-ÿ '-]+" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="mail">Mail</label>
                  <input type="email" name="mail" maxlength="45" required="required" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="mdp1">Créer mot de passe</label>
                  <input type="password" name="mdp1" required="required" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="mdp2">Confirmer mot de passe</label>
                  <input type="password" name="mdp2" required="required" class="form-control"/><br />
                </div>

                <label class="radio-inline"><input type="radio" name="statut" value = "V" checked>Vendeur</label>
                <label class="radio-inline"><input type="radio" name="statut" value = "A">Administrateur</label>

                <div class="form-group">
                <label for="point">Associer point de retrait:</label>
                <select class="form-control" name="point">
                  <?php 

                    foreach ($points as $p) {
                      echo "<option value=\"".$p['point_id']."\">".$p['point_nom']."</option>";
                    }
                  ?>
                </div>


                <input type="submit" name="submit" value="Créer un compte" class="btn btn-success mt-3" />
              </form>

            </div>

         </div>
         
       </div>