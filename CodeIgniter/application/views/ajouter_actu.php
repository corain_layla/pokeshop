      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
            Poster une actualité
            </div>
            <div class="card-body">
              <?php 

                echo smiley_js();

                echo $erreur;

                echo validation_errors();
                echo form_open('ajouter_actu'); ?>
                <div class="form-group">
                  <label for="titre">Titre actualité</label>
                  <input type="input" name="titre" maxlength="45" required="required" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="texte">Texte</label>
                  <textarea type="input" name="texte" id="texte" required="required" class="form-control"></textarea><br >
                </div>

                <small>Cliquer pour ajouter un smiley!</small>

                 <?php echo $smiley_table; ?>

                <input type="submit" name="submit" value="Poster l'actualité" class="btn btn-success mt-5" />
              </form>

            </div>

         </div>
         
       </div>