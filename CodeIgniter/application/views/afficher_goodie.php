      <div class="col-lg-9">

        <div class="card mt-4">

          <?php
          echo"<img class=\"card-img-top img-fluid\" src=\"".$this->config->item('base_url')."/images/".$goodie->goo_image."\">
          <div class=\"card-body\">
            <h3 class=\"card-title\">".$goodie->goo_nom."</h3>
            <h4>".$goodie->goo_prix." €</h4>
            <p class=\"card-text\"><small>Stocks disponibles : ".$goodie->goo_stock." unités</small></p>
            <p class=\"card-text\">".$goodie->goo_description."</p>
          </div>";

          ?>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            Ajouter au panier
          </div>
          <div class="card-body">
            <?php

              echo 'Quantité de ce goodie déjà dans le panier: '.$nb;

              if ($nb != $goodie->goo_stock) {

                echo validation_errors();
                echo form_open('ajouter_au_panier');
                echo "<div class=\"form-group mt-2\">
                  <label for=\"nb\">Quantité</label>
                  <select class=\"form-control\" name=\"nb\">";

                  for ($i=1;$i<=$goodie->goo_stock - $nb;$i++) {
                    echo"<option value=\"$i\">".$i."</option>";
                  }

                  echo"</select>
                </div>
                <input type=\"hidden\" value=\"".$goodie->goo_id."\" name=\"id\">
                <input type=\"hidden\" value=\"".$goodie->goo_prix."\" name=\"prix\">
                <input type=\"hidden\" value=\"".$goodie->goo_nom."\" name=\"nom\">
                <input type=\"submit\" name=\"submit\" value=\"Ajouter au panier\" class=\"btn btn-success\" />
                </form>";
              }

              
            ?>
            
          </div>
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col-lg-9 -->