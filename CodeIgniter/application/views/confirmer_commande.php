      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
            Mes informations personnelles
            </div>
            <div class="card-body">
              <?php 

                echo validation_errors();
                echo form_open('confirmer'); ?>

                <div class="form-group">
                  <label for="nom">Nom</label>
                  <input type="input" name="nom" maxlength="45" required="required" pattern="[A-Za-zÀ-ÿ '-]+" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="prenom">Prénom</label>
                  <input type="input" name="prenom" maxlength="45" required="required" pattern="[A-Za-zÀ-ÿ '-]+" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="id">Mail</label>
                  <input type="email" name="mail" maxlength="45" required="required" class="form-control"/><br />
                </div>

                <div class="form-group">
                <label for="point">Choisir point de retrait:</label>
                <select class="form-control" name="point">
                  <?php 

                    foreach ($points as $p) {
                      echo "<option value=\"".$p['point_id']."\">".$p['point_nom']."</option>";
                    }
                  ?>
                </div>

                <input type="submit" name="submit" value="Confirmer ma commande" class="btn btn-success mt-5" />
              </form>

            </div>

         </div>
         
       </div>