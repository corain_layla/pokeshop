      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            Se connecter
          </div>
          <div class="card-body">

            <p>Vous êtes administrateur ou gérant d'un point de retrait? connectez-vous.</p>

            <?php echo $erreur;
            
              echo validation_errors();
              echo form_open('connexion'); ?>
                <div class="form-group">
                  <label for="id">Pseudo</label>
                  <input type="input" name="id" maxlength="16" required="required" class="form-control"/><br />
                </div>
                <div class="form-group">
                  <label for="mdp">Mot de passe</label>
                  <input type="password" name="mdp" required="required" class="form-control"/><br />
                </div>

                <input type="submit" name="submit" value="Se connecter" class="btn btn-success" />
              </form>

          </div>

        </div>
        
      </div>