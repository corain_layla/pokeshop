      <div class="col-lg-9">

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
            Modifier mon profil
            </div>
            <div class="card-body">
              <?php 

                echo $erreur;

                echo validation_errors();
                echo form_open('modifier_profil');
                  echo "<div class=\"form-group\">
                    <label for=\"nom\">Nom</label>
                    <input type=\"input\" name=\"nom\" value=\"".$result->pro_nom."\" maxlength=\"45\" required=\"required\" pattern=\"[A-Za-zÀ-ÿ '-]+\" class=\"form-control\"/><br />
                  </div>
                  <div class=\"form-group\">
                    <label for=\"prenom\">Prénom</label>
                    <input type=\"input\" name=\"prenom\" value=\"".$result->pro_prenom."\" maxlength=\"45\" required=\"required\" pattern=\"[A-Za-zÀ-ÿ '-]+\" class=\"form-control\"/><br />
                  </div>
                  <div class=\"form-group\">
                    <label for=\"mail\">Mail</label>
                    <input type=\"email\" name=\"mail\" value=\"".$result->pro_mail."\" maxlength=\"45\" required=\"required\" class=\"form-control\"/><br />
                  </div>
                  <div class=\"form-group\">
                    <label for=\"mdp1\">Créer nouveau mot de passe</label>
                    <input type=\"password\" name=\"mdp1\" required=\"required\" class=\"form-control\"/><br />
                  </div>
                  <div class=\"form-group\">
                    <label for=\"mdp2\">Confirmer nouveau mot de passe</label>
                    <input type=\"password\" name=\"mdp2\" required=\"required\" class=\"form-control\"/><br />
                  </div>
                  <input type=\"submit\" name=\"submit\" value=\"Modifier mon profil\" class=\"btn btn-success\" />
                </form>
              </br>
              <a href=\"".$this->config->item('base_url')."/index.php/accueilGestionnaire/afficher/0\">"; ?><button type="button" class="btn btn-primary">Annuler</button></a>
            </div>
          </div>
        </div>