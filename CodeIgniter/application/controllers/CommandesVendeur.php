<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class CommandesVendeur extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function lister($code) {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('etat', 'etat', 'required');
			$this->form_validation->set_rules('id', 'id', 'required');


			if ($_SESSION['statut'] == 'V') {

				if($this->form_validation->run() == FALSE) {

					$data['commande'] = $this->db_model->get_commandes_vendeur();

					if ($code == 0) {
						$data['msgtop']='';
						$data['msgbottom']='';

					} else if ($code==1) {

						$data['msgtop']='';
						$data['msgbottom']="<div class=\"alert alert-warning mt-5\">
								          <strong>Attention!</strong> Vous ne pouvez pas annuler cette action. Confirmer?
								          <a href=\"".$this->config->item('base_url')."/index.php/commandesVendeur/lister/0\"><button type=\"button\" class=\"btn btn-primary\">Annuler</button></a>
								          <a href=\"".$this->config->item('base_url')."/index.php/commandesVendeur/lister/2\"><button type=\"button\" class=\"btn btn-success\">Confirmer</button></a>

								        </div>";

					} else if ($code == 2) {

						$data['msgtop']="<div class=\"alert alert-success mt-5\">
								          <strong>Succès!</strong> L'état de la commande a été modifié.
								        </div>";;
						$data['msgbottom']='';

						if ( (isset($_SESSION['commande'])) && (isset ($_SESSION['etat'])) ) {

							$this->db_model->update_etat_commande($_SESSION['commande'], $_SESSION['etat']);

							if ($_SESSION['etat'] == 'A') {

								$this->db_model->update_stock_del($_SESSION['commande']);
							}

							$array_items = array('commande', 'etat');
							$this->session->unset_userdata($array_items);

							$data['commande'] = $this->db_model->get_commandes_vendeur();

						} else {

							redirect($this->config->item('base_url')."/index.php/commandesVendeur/lister/0");

						}

						
					}

					$this->load->view('templates/haut');
					$this->load->view('templates/colonne_vendeur');
					$this->load->view('commandes_liste',$data);
					$this->load->view('templates/bas');

				} else {

					$session_data = array('commande' => $this->input->post('id'), 'etat' => $this->input->post('etat') );
					$this->session->set_userdata($session_data);

					redirect($this->config->item('base_url')."/index.php/commandesVendeur/lister/1");
					
				}

				
			} else {

				redirect($this->config->item('base_url'));
			}
		}

		public function afficher($id) {

			if ($_SESSION['statut'] == 'V') {

				$data['result'] = $this->db_model->get_commande($id);
				$data['result1'] = $this->db_model->get_goodies_commande($id);

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_vendeur');
				$this->load->view('afficher_commande',$data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url'));
			}
		}
	}
?>