<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class GalerieOriginaux extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function lister() {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('cat', 'cat', 'required');

			if ($this->form_validation->run() == FALSE) {

				$data['originaux'] = $this->db_model->get_all_originaux();
				$data['categories'] = $this->db_model->get_all_categories();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('originaux', $data);
				$this->load->view('templates/bas');

			} else {

				if($this->input->post('cat') == 0) {
					$data['originaux'] = $this->db_model->get_all_originaux();
				} else {
					$data['originaux'] = $this->db_model->get_originaux_cat($this->input->post('cat'));
				}

				$data['categories'] = $this->db_model->get_all_categories();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('originaux', $data);
				$this->load->view('templates/bas');

			}

		}
	}
?>