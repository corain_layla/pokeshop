<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class AccueilGestionnaire extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url');
			$this->load->library('cart');
		}

		public function afficher($error) {

			if (isset($_SESSION['username'])) {

				$data['result'] = $this->db_model->get_profil(addslashes($_SESSION['username']));

				if($error == 0) {
					$data['erreur'] = "";
				} else if($error == 1) {
					$data['erreur'] = "<div class=\"alert alert-success\">
								          <strong>Succès! </strong> Vos informations ont bien été mises à jour.
								        </div>";
				} else {
					$data['erreur'] = "<div class=\"alert alert-info\">
								          Bienvenue sur votre espace gestionnaire, <strong>".stripslashes($_SESSION['username'])."</strong>!
								        </div>";
				}

				$this->load->view('templates/haut');
				if ($_SESSION['statut'] == 'V') {
					$this->load->view('templates/colonne_vendeur');
				} else if ($_SESSION['statut'] == 'A'){
					$this->load->view('templates/colonne_admin');
				}
				$this->load->view('accueil_gestionnaire',$data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url'));
			}
		}

		public function modifier($error) {

			if (isset($_SESSION['username'])) {

				$this->load->helper('form');
				$this->load->library('form_validation');
				$this->form_validation->set_rules('mdp1', 'mdp1', 'required');
				$this->form_validation->set_rules('mdp2', 'mdp2', 'required');
				$this->form_validation->set_rules('nom', 'nom', 'required');
				$this->form_validation->set_rules('prenom', 'prenom', 'required');
				$this->form_validation->set_rules('mail', 'mail', 'required');

				if ($this->form_validation->run() == FALSE) {

					$data['result'] = $this->db_model->get_profil(addslashes($_SESSION['username']));

					if($error == 0) {
						$data['erreur'] = "";
					} else {
						$data['erreur'] = "<div class=\"alert alert-danger\">
									          <strong>Erreur !</strong> Echec de la confirmation du nouveau mot de passe. Veuillez réessayer.
									        </div>";
					}

					$this->load->view('templates/haut');
					if ($_SESSION['statut'] == 'V') {
						$this->load->view('templates/colonne_vendeur');
					} else {
						$this->load->view('templates/colonne_admin');
					}
					$this->load->view('modifier_profil', $data);
					$this->load->view('templates/bas');

				} else {

					if(htmlspecialchars(addslashes($this->input->post('mdp1'))) == htmlspecialchars(addslashes($this->input->post('mdp2')))) {

						$this->db_model->update_profil_compte();

						redirect($this->config->item('base_url').'/index.php/accueilGestionnaire/afficher/1');

					} else {

						redirect($this->config->item('base_url').'/index.php/accueilGestionnaire/modifier/1');
					}
					
				}
				
			} else {

				redirect($this->config->item('base_url'));
			}
		}
	}
?>