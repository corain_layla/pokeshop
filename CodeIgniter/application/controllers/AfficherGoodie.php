<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class AfficherGoodie extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function afficher($id) {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nb', 'nb', 'required');
			$this->form_validation->set_rules('id', 'id', 'required');
			$this->form_validation->set_rules('prix', 'prix', 'required');
			$this->form_validation->set_rules('nom', 'nom', 'required');

			if ($this->form_validation->run() == FALSE) {

				if ($id != 0) {

					$data['goodie'] = $this->db_model->get_goodie($id);

					$data['nb'] = 0;

					foreach ($this->cart->contents() as $items) {
						if($id == $items['id']) {
							$data['nb'] = $items['qty'];
						}
					}

					$this->load->view('templates/haut');
					$this->load->view('templates/colonne_client');
					$this->load->view('afficher_goodie', $data);
					$this->load->view('templates/bas');
				}

			} else {

				$data = array(
				        'id'      => $this->input->post('id'),
				        'qty'     => $this->input->post('nb'),
				        'price'   => $this->input->post('prix'),
				        'name'    => $this->input->post('nom'),
				);

				$this->cart->insert($data);

				redirect($this->config->item('base_url').'/index.php/panier/afficher');

			}

		}
	}
?>