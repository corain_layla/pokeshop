<?php
	class RecapCommande extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url');
			$this->load->library('cart');
		}

		public function afficher($error) {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('com', 'com', 'required');
			$this->form_validation->set_rules('cli', 'cli', 'required');

			if ($this->form_validation->run() == FALSE) {

				if($error == 0) {
					$data['erreur'] = "";

					$array_items = array('commande', 'client', 'etat');
					$this->session->unset_userdata($array_items);

				} else if ($error == 1) {
					$data['erreur'] = "<div class=\"alert alert-danger\">
								          <strong>Codes invalides!</strong> La commande que vous cherchez n'existe pas.
								        </div>";

				} else if ($error == 2) {

					$data['erreur'] = "<div class=\"alert alert-success\">
								          <strong>Succès!</strong> Votre commande a bien été supprimée.
								        </div>";

				}

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('recap_commande', $data);
				$this->load->view('templates/bas');

			} else {


				if ($this->db_model->generate_recap($this->input->post('com'), $this->input->post('cli'))) {

					$session_data = array('commande' => $this->input->post('com'), 'client' => $this->input->post('cli') );
					$this->session->set_userdata($session_data);

					redirect($this->config->item('base_url').'/index.php/recapCommande/recap/0');

				} else {

					redirect($this->config->item('base_url').'/index.php/recapCommande/afficher/1');
				}
				
			}
		}

		public function recap($code) {

			if (isset($_SESSION['commande'])) {

				if ($code == 0) {

					$data['msgbottom']='';

				} else if ($code == 1) {

					$data['msgbottom']="<div class=\"alert alert-warning mt-5\">
								          <strong>Attention!</strong> Vous ne pouvez pas annuler cette action. Confirmer?
								          <a href=\"".$this->config->item('base_url')."/index.php/recapCommande/recap/0\"><button type=\"button\" class=\"btn btn-primary\">Annuler</button></a>
								          <a href=\"".$this->config->item('base_url')."/index.php/recapCommande/recap/2\"><button type=\"button\" class=\"btn btn-success\">Confirmer</button></a>
									        </div>";

				} else if ($code == 2) {

					$this->db_model->update_stock_del($_SESSION['commande']);
					$this->db_model->delete_commande($_SESSION['commande']);

					$array_items = array('commande', 'client');
					$this->session->unset_userdata($array_items);
					
					redirect($this->config->item('base_url').'/index.php/recapCommande/afficher/2');

				}

				$data['result'] = $this->db_model->get_commande($_SESSION['commande']);
				$data['result1'] = $this->db_model->get_goodies_commande($_SESSION['commande']);

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('afficher_commande', $data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url').'/index.php/recapCommande/afficher/0');

			}

			
		}
	}
?>