<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Deconnexion extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->helper('url');
			$this->load->library('cart');
		}

		public function logout() {

			$array_items = array('username', 'statut', 'commande', 'etat', 'client');
			$this->session->unset_userdata($array_items);

			redirect($this->config->item('base_url')."/index.php/connexion/afficher/0");
		}
	}
?>
