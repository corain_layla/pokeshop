<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Panier extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function afficher() {

			$this->load->helper('form');

			$this->load->view('templates/haut');
			$this->load->view('templates/colonne_client');
			$this->load->view('cart');
			$this->load->view('templates/bas');

		}

		public function MaJ() {

			$this->load->helper('form');

			//for each item in the cart
			for($i=0;$i<$this->input->post('n');$i++) {

				$stock = $this->db_model->get_stock($this->input->post($i.'[id]'));

				//whether the stock is enough or not
				if ($this->input->post($i.'[qty]') < $stock->goo_stock) {
					$QTY = $this->input->post($i.'[qty]');
				} else {
					$QTY = $stock->goo_stock;
				}

				//update query
				$data = array(
				        'rowid' => $this->input->post($i.'[rowid]'),
				        'qty'   => $QTY,
					);
				$this->cart->update($data);
			}

			$this->load->view('templates/haut');
			$this->load->view('templates/colonne_client');
			$this->load->view('cart');
			$this->load->view('templates/bas');
		}

		public function DeleteItem ($rowid) {

			$this->cart->remove($rowid);

			redirect($this->config->item('base_url')."/index.php/panier/afficher");
		}
	}
?>