<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class AjouterCompte extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url');
			$this->load->library('cart');
		}

		public function afficher($error) {

			if ($_SESSION['statut'] == 'A') {

				$this->load->helper('form');
				$this->load->library('form_validation');
				$this->form_validation->set_rules('id', 'id', 'required');
				$this->form_validation->set_rules('mdp1', 'mdp1', 'required');
				$this->form_validation->set_rules('mdp2', 'mdp2', 'required');
				$this->form_validation->set_rules('nom', 'nom', 'required');
				$this->form_validation->set_rules('prenom', 'prenom', 'required');
				$this->form_validation->set_rules('mail', 'mail', 'required');

				if ($this->form_validation->run() == FALSE) {

					if($error == 0) {
						$data['erreur'] = "";
					} else if($error == 1) {
						$data['erreur'] = "<div class=\"alert alert-danger\">
									          <strong>Erreur !</strong> La confirmation du mot de passe a échoué. Veuillez réessayer.
									        </div>";
					} else if($error == 2) {
						$data['erreur'] = "<div class=\"alert alert-danger\">
									          <strong>Erreur !</strong> Le pseudo que vous avez entré est déja utilisé.
									        </div>";
					}

					$data['points'] = $this->db_model->get_all_points();

					$this->load->view('templates/haut');
					$this->load->view('templates/colonne_admin');
					$this->load->view('ajouter_compte', $data);
					$this->load->view('templates/bas');

				} else {

					if($this->db_model->get_compte_check_creation()) {

						if(htmlspecialchars(addslashes($this->input->post('mdp1'))) == htmlspecialchars(addslashes($this->input->post('mdp2')))) {

							$this->db_model->set_compte();
							$this->db_model->set_profil();

							redirect($this->config->item('base_url').'/index.php/compteListe/lister/1');

						} else {

							redirect($this->config->item('base_url').'/index.php/ajouterCompte/afficher/1');
						}
					} else {

						redirect($this->config->item('base_url').'/index.php/ajouterCompte/afficher/2');
					}
					
				}

			} else {

				redirect($this->config->item('base_url'));

			}
		}
	}
?>