<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class ConfirmerCommande extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function afficher() {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nom', 'nom', 'required');
			$this->form_validation->set_rules('prenom', 'prenom', 'required');
			$this->form_validation->set_rules('mail', 'mail', 'required');
			$this->form_validation->set_rules('point', 'point', 'required');

			if ($this->cart->total_items() < 1) {

				redirect($this->config->item('base_url')."/index.php/panier/afficher");
			}

			if ($this->form_validation->run() == FALSE) {

				$data['points'] = $this->db_model->get_all_points();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('confirmer_commande', $data);
				$this->load->view('templates/bas');

			} else {

				$n = 0;

				while ($this->db_model->check_mail(md5(htmlspecialchars(addslashes($this->input->post('mail').$n))))) {

					$n++;
				}

				//pour générer un code client et code commande
				$MD5 = md5(htmlspecialchars(addslashes($this->input->post('mail').$n)));

				//insert dans t_commande
				$this->db_model->set_commande($MD5, $this->cart->total());

				//insert dans la table de jointure
                foreach ($this->cart->contents() as $items) {               
                	$this->db_model->set_contenu($MD5, $items['id'], $items['qty']);
                }

                $session_data = array('commande' => substr($MD5, 0, 20), 'client' => substr($MD5, 20, 8));
				$this->session->set_userdata($session_data);

				//update des stocks
				$this->db_model->update_stock_com($_SESSION['commande']);

				//on vide le panier
				$this->cart->destroy();

                redirect($this->config->item('base_url')."/index.php/recapCommande/recap/0");

			}

		}

	}
?>