<?php
	class Connexion extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url');
			$this->load->library('cart');
		}

		public function afficher($error) {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('id', 'id', 'required');
			$this->form_validation->set_rules('mdp', 'mdp', 'required');

			if ($this->form_validation->run() == FALSE) {

				if($error == 0) {
					$data['erreur'] = "";
				} else {
					$data['erreur'] = "<div class=\"alert alert-danger\">
								          <strong>Erreur ! </strong> Identifiant, mot de passe ou statut invalide.
								        </div>";
				}

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('connexion', $data);
				$this->load->view('templates/bas');

			} else {

				$username = htmlspecialchars(addslashes($this->input->post('id')));
				$password = htmlspecialchars(addslashes($this->input->post('mdp')));

				if($this->db_model->connect_compte($username,$password)) {

					$status = $this->db_model->get_statut_connexion($username);

					$session_data = array('username' => $username, 'statut' => $status->statut);
					$this->session->set_userdata($session_data);

					redirect($this->config->item('base_url').'/index.php/accueilGestionnaire/afficher/2');

				} else {

					redirect($this->config->item('base_url').'/index.php/connexion/afficher/1');
				}

			}
		}
	}
?>