<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Accueil extends CI_Controller {

		public function __construct () {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url');
			$this->load->helper('smiley');
			$this->load->library('cart');
		}

		public function afficher() {

			$data['actus'] = $this->db_model->get_all_actualite();

			$this->load->view('templates/haut');
			$this->load->view('templates/colonne_client');
			$this->load->view('page_accueil',$data);
			$this->load->view('templates/bas');
		}
	}
?>