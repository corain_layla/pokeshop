<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class CommandesListe extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function lister() {

			if ($_SESSION['statut'] == 'A') {

				$data['commande'] = $this->db_model->get_all_commandes();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_admin');
				$this->load->view('commandes_liste',$data);
				$this->load->view('templates/bas');
			} else {

				redirect($this->config->item('base_url'));
			}
		}

		public function afficher($id) {
			
			if (($_SESSION['statut'] == 'A') || ($_SESSION['statut'] == 'V') ) {

				$data['result'] = $this->db_model->get_commande($id);
				$data['result1'] = $this->db_model->get_goodies_commande($id);

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_admin');
				$this->load->view('afficher_commande',$data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url'));
			}
		}
	}
?>