<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class ActusListe extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url');
			$this->load->helper('url_helper');
			$this->load->helper('smiley');
			$this->load->library('table');
			$this->load->library('cart');
		}

		public function lister() {

			if ($_SESSION['statut'] == 'A') {

				$data['result'] = $this->db_model->get_all_actualite();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_admin');
				$this->load->view('actus_liste',$data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url'));
			}
		}

		public function ajouter($error) {

			if ($_SESSION['statut'] == 'A') {

				$this->load->helper('form');
				$this->load->library('form_validation');
				$this->form_validation->set_rules('titre', 'titre', 'required');
				$this->form_validation->set_rules('texte', 'texte', 'required');

				if ($this->form_validation->run() == FALSE) {

					if($error == 0) {
						$data['erreur'] = "";
					} else if($error == 1) {
						$data['erreur'] = "<div class=\"alert alert-danger\">
									          <strong>Erreur !</strong> La confirmation du mot de passe a échoué. Veuillez réessayer.
									        </div>";
					} else if($error == 2) {
						$data['erreur'] = "<div class=\"alert alert-danger\">
									          <strong>Erreur !</strong> Le pseudo que vous avez entré est déja utilisé.
									        </div>";
					}

					$image_array = get_clickable_smileys($this->config->item('base_url').'/images/smileys', 'texte');
		            $col_array = $this->table->make_columns($image_array, 20);

		            $data['smiley_table'] = $this->table->generate($col_array);

		            //$data['categories'] = $this->db_model->get_all_categories();

					$this->load->view('templates/haut');
					$this->load->view('templates/colonne_admin');
					$this->load->view('ajouter_actu',$data);
					$this->load->view('templates/bas');

				} else {

					$this->db_model->set_actu();

					redirect($this->config->item('base_url').'/index.php/actusListe/lister');
				}

			} else {

				redirect($this->config->item('base_url'));
			}

		}
	}
?>