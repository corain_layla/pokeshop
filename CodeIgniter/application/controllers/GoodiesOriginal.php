<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class GoodiesOriginal extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function afficher($id) {

			$data['result'] = $this->db_model->get_goodies_original($id);
			$data['original'] = $this->db_model->get_originaux($id);

			$this->load->view('templates/haut');
			$this->load->view('templates/colonne_client');
			$this->load->view('goodies_original', $data);
			$this->load->view('templates/bas');

		}
	}
?>