<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class RetraitListe extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function lister() {

			if ($_SESSION['statut'] == 'A') {

				$data['result'] = $this->db_model->get_all_points_pseudo();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_admin');
				$this->load->view('retrait_liste',$data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url'));
			}
		}
	}
?>