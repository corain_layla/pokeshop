<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class CompteListe extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function lister($error) {

			if ($_SESSION['statut'] == 'A') {

				$data['pseudos'] = $this->db_model->get_all_compte();
				$data['total'] = $this->db_model->get_total_compte();

				if($error == 0) {
					$data['erreur'] = "";
				} else {
					$data['erreur'] = "<div class=\"alert alert-success\">
								          <strong>Succès!</strong> Nouveau compte ajouté.
								        </div>";
				}

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_admin');
				$this->load->view('compte_liste',$data);
				$this->load->view('templates/bas');

			} else {

				redirect($this->config->item('base_url'));
			}
		}
	}
?>