<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class GalerieGoodies extends CI_Controller {

		public function __construct() {

			parent::__construct();
			$this->load->model('db_model');
			$this->load->helper('url_helper');
			$this->load->library('cart');
		}

		public function lister() {

			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('type', 'type', 'required');

			if ($this->form_validation->run() == FALSE) {

				$data['types'] = $this->db_model->get_all_type_goodies();
				$data['goodies'] = $this->db_model->get_all_goodies();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('goodies_type', $data);
				$this->load->view('templates/bas');

			} else {

				if($this->input->post('type') == 0) {
					$data['goodies'] = $this->db_model->get_all_goodies();
				} else {
					$data['goodies'] = $this->db_model->get_goodies_type($this->input->post('type'));
				}

				$data['types'] = $this->db_model->get_all_type_goodies();

				$this->load->view('templates/haut');
				$this->load->view('templates/colonne_client');
				$this->load->view('goodies_type', $data);
				$this->load->view('templates/bas');

			}

		}
	}
?>