insert into t_compte values ('responsable',SHA2('******HellIsEmptyAndAllTheDevilsAreThere!#69', 256), 'A', null);
insert into t_profil values ('CORAIN', 'Layla', 'corain.gaillard.layla@gmail.com', 'responsable');

insert into t_point_retrait values (null, 'Super U Mac Orlan', '5 rue Saint Exupery');
insert into t_point_retrait values (null, 'ASPI SHOP', '18 rue Langevin');
insert into t_point_retrait values (null, 'Kornog Tattoo', '169 boulevard Jean Jaures');
insert into t_point_retrait values (null, 'Comptoir Kerjean', '45 rue de Siam');

insert into t_compte values ('Lolo', SHA2('roukmouteHellIsEmptyAndAllTheDevilsAreThere!#69', 256), 'V', 1);
insert into t_profil values ('CONSANTIN', 'Laurie', 'constantin.laurie-may@univ-brest.fr', 'Lolo');

insert into t_compte values ('Pogona', SHA2('mufasaHellIsEmptyAndAllTheDevilsAreThere!#69', 256), 'V', 2);
insert into t_profil values ('BRIANT', 'Alexandre', 'mufasa@gmail.com', 'Pogona');

insert into t_compte values ('Larna', SHA2('2000HellIsEmptyAndAllTheDevilsAreThere!#69', 256), 'V', 3);
insert into t_profil values ('COAT', 'Arthur', 'coat.arthur@univ-brest.fr', 'Larna');

insert into t_compte values ('Quendeux', SHA2('moustacheHellIsEmptyAndAllTheDevilsAreThere!#69', 256), 'V', 4);
insert into t_profil values ('AUTRET', 'Quentin', 'autret.quentin@univ-brest.fr', 'Quendeux');

insert into t_compte values ('Lilou', SHA2('yoloHellIsEmptyAndAllTheDevilsAreThere!#69', 256), 'V', 3);
insert into t_profil values ('CORAIN', 'Layla', 'layla.corain@univ-brest.fr', 'Lilou');

insert into t_categorie values (null, 'Pokémons');
insert into t_categorie values (null, 'Objets');
insert into t_categorie values (null, 'Pokéshop');

insert into t_original values (null, 'Bulbizarre', "Retrouvez l'adorable starter de type Plante de la première génération Pokémon! Numéro officiel Pokédex: 001.", "originaux/bulbasaur.gif", 1, 'responsable');
insert into t_original values (null, 'Pikachu', "Le Pokémon le plus populaire de la saga Pokémon débarque au Pokéshop! Numéro officiel Pokédex: 025", "originaux/pikachu.gif", 1, 'responsable');
insert into t_original values (null, 'Malosse', "Malosse, le Pokémon préféré de la développeuse du Pokéshop! Son double type Feu, Ténêbres, et son air de gentil Rottweiler va vous faire craquer. Numéro officiel Pokédex: 228.", "originaux/houndour.gif", 1, 'responsable');
insert into t_original values (null, 'Tous les Pokémons', "Il y a près de 1000 Pokémons enregistrés ! Attrapez-les tous!", "originaux/allpkmn.gif", 1, 'responsable');
insert into t_original values (null, 'Pokéball', "Découvrez tous nos goodies Pokéball! ", "originaux/pokeball.gif", 2, 'responsable');
insert into t_original values (null, 'Pokédex', "Vous rêvez de devenir un dresseur? N'attendez pas Professeur Chen. Achetez-vous votre Pokédex!", "originaux/pokedex.gif", 2, 'responsable');
insert into t_original values (null, 'Pokéshop', "Le Pokéshop est une référence dans la vente de goodies. Profitez de nos marchandises exclusives!", "logo.png", 3, 'responsable');

insert into t_type_goodie values (null, 'Peluche');
insert into t_type_goodie values (null, 'Vêtement');
insert into t_type_goodie values (null, 'Porte clef');
insert into t_type_goodie values (null, 'Coque téléphone');
insert into t_type_goodie values (null, 'Livre');
insert into t_type_goodie values (null, 'Jeux');


insert into t_goodies values (null, 'Peluche Bulbizarre', 'La peluche officielle du starter Plante de la première génération!', "goodies/peluchebulbi.png", 25, 100, 1);
insert into tj_liste values (1, 1);

insert into t_goodies values (null, 'T-shirt Bulbizarre', 'Affichez fièrement votre amour de Bulbi. (Coupe unisexe)', "goodies/tshirtbulbi.jpg", 20, 100, 2);
insert into tj_liste values (2, 1);

insert into t_goodies values (null, 'T-shirt Pokéshop Malosse', 'Le Pokéshop est fier d\'avoir Malosse pour mascotte! (Coupe Unisexe)', "goodies/tshirt_malosse.png", 20, 100, 2);
insert into tj_liste values (3, 3);
insert into tj_liste values (3, 4);
insert into tj_liste values (3, 7);

insert into t_goodies values (null, 'Porte-clef Bulbizarre', 'Emportez Bulbizarre dans vos aventures quotidiennes. Il protégera vos clefs!', "goodies/porteclefbulbi.jpg", 4, 100, 3);
insert into tj_liste values (4, 1);

insert into t_goodies values (null, 'Jouet Pokéball Pikachu', 'Jouet en plastique Pokéball et figurine: attrapez Pikachu pour de vrai! D\'autres figurines bientôt disponibles!', "goodies/jouetpokeball.jpg", 15, 100, 6);
insert into tj_liste values (5, 2);
insert into tj_liste values (5, 5);

insert into t_goodies values (null, 'Peluche Salamèche', 'La peluche officielle du starter Feu de la première génération!', "goodies/peluchesalameche.jpg", 25, 100, 1);
insert into tj_liste values (6, 4);

insert into t_goodies values (null, 'T-shirt Malosse', '\"Je suis un Pokémon Rottweiler trop mignon! Portez moi sur un t-shirt!\" (Coupe unisexe)', "goodies/tshirtmalosse.jpg", 20, 100, 2);
insert into tj_liste values (7, 3);

insert into t_goodies values (null, 'T-shirt Pokéshop Bulbizarre', 'Le Pokéshop est fier d\'avoir Bulbizarre pour mascotte! (Coupe Unisexe)', "goodies/tshirt_bulbi.png", 20, 100, 2);
insert into tj_liste values (8, 1);
insert into tj_liste values (8, 4);
insert into tj_liste values (8, 7);

insert into t_goodies values (null, 'Porte-clef Osselait', 'Osselait a perdu sa maman! Il va tenir compagnie à vos clef en attendant de la retrouver.', "goodies/porteclefosselait.jpg", 4, 100, 3);
insert into tj_liste values (9, 4);

insert into t_goodies values (null, 'Peluche Malosse', 'La peluche officielle de Malosse, le Pokémon préféré de la développeuse!', "goodies/peluchemalosse.jpg", 25, 100, 1);
insert into tj_liste values (10, 3);

insert into t_goodies values (null, 'Sweatshirt Pokéball', 'CATCHy dites vous? On voit pas de quoi vous parlez. Taille M (Forme unisexe)', "goodies/sweatpokeball.jpg", 45, 100, 2);
insert into tj_liste values (11, 5);

insert into t_goodies values (null, 'Coque Pokéball', 'Protégez votre smartphone avec cette coque motif Pokéball!', "goodies/coquepokeball.jpg", 20, 100, 4);
insert into tj_liste values (12, 5);

insert into t_goodies values (null, 'Porte-clef Pokéball', 'Emportez cette mini-pokéball en porte clef partout avec vous!', "goodies/porteclefpokeball.png", 4, 100, 3);
insert into tj_liste values (13, 5);

insert into t_goodies values (null, 'Nintendo Pokéball Plus', 'Vous jouez à Pokémon GO? Aux jeux Pokémon sur la Nintendo Switch? Cet accessoire va changer votre expérience de jeu! Voir site officiel de Nintendo pour plus d\'informations.', "goodies/pokeballplus.jpg", 90, 100, 6);
insert into tj_liste values (14, 5);

insert into t_goodies values (null, 'Coque Pokédex', 'Protégez votre smartphone avec cette coque très, très geek.', "goodies/coquepokedex.jpg", 20, 100, 4);
insert into tj_liste values (15, 4);
insert into tj_liste values (15, 6);

insert into t_goodies values (null, 'Le Pokédex Complet', 'Marre de Poképédia? Tous les Pokémons. Dans un seul livre. Ce livre est pour vous!', "goodies/pokedexlivre.png", 30, 100, 5);
insert into tj_liste values (16, 4);
insert into tj_liste values (16, 6);

insert into t_goodies values (null, 'Pokédex électronique', 'Vous l\'avez rêvé? Il est là! Le tout premier Pokédex REEL ! Exclusivement sur Pokéshop!', "goodies/pokedexjouet.png", 40, 100, 6);
insert into tj_liste values (17, 4);
insert into tj_liste values (17, 6);

insert into t_goodies values (null, 'Caleçon Pikachu', 'Vous êtes nerd et désirez porter votre amour des Pokémons au plus proche de vous? Ce caleçon est fait pour vous.', "goodies/caleconpikachu.png", 10, 100, 2);
insert into tj_liste values (18, 2);

insert into t_goodies values (null, 'Peluche Pikachu', 'La peluche officielle du Pokémon le plus populaire!', "goodies/peluchepikachu.png", 25, 100, 1);
insert into tj_liste values (19, 2);

insert into t_goodies values (null, 'T-shirt Pokéshop Pikachu', 'Le Pokéshop est fier d\'avoir Pikachu pour mascotte! (Coupe Unisexe)', "goodies/tshirt_pikachu.png", 20, 100, 2);
insert into tj_liste values (20, 2);
insert into tj_liste values (20, 4);
insert into tj_liste values (20, 7);

insert into t_actualite values (null, 'Ouverture du site', 'Bienvenue sur le Pokéshop! Nous sommes ravis de vous accueillir sur notre nouveau site de commandes de goodies Pokémon! Seul le Back End est disponible pour le moment. Contactez un administrateur afin d\'obtenir votre compte vendeur!', NOW()-INTERVAL 2 DAY, 'responsable');
insert into t_actualite values (null, 'Caractères spéciaux', 'Notez qu\'afin de protéger ses champs d\'entrée, Pokéshop n\'accepte pas tous les caractères spéciaux. Caractères spéciaux actuellement acceptés: *, \', -. D\'autres seront ajoutés prochainement.', NOW()-INTERVAL 1 DAY, 'responsable');
insert into t_actualite values (null, 'Nouveaux t-shirts!', 'Découvrez nos t-shirt floqués du logo Pokéshop, et de votre Pokémon préféré! Au choix, Pikachu, Bulbizarre et Malosse!', NOW(), 'responsable');
insert into t_actualite values (null, 'Nouveau ! Les smileys :coolsmile:', 'Il est désormais possible d\'écrire des smileys dans les actualités! Amusez-vous bien! ;-P Attention, cette fonctionnalité n\'est disponible QUE pour les actualités :exclaim:', NOW()+INTERVAL 1 SECOND, 'responsable');
insert into t_actualite values (null, 'Panier', 'La gestion du panier et l\'ajout de commandes sont fonctionnels! Il est temps de de dévaliser nos stocks!', NOW()+INTERVAL 2 SECOND, 'responsable');
insert into t_actualite values (null, 'Mise à jour des patterns', 'Il est désormais possible d\'utiliser tous les caractères spéciaux dans les champs d\'entrée du Pokéshop!  :lol:  Attention: Exception sur certains champs (Nom, prénom, codes client et commande, et bien sûr adresses e-mail)',NOW()+INTERVAL 3 SECOND,'responsable');

insert into tj_tag values (1, 7);
insert into tj_tag values (2, 7);
insert into tj_tag values (3, 1);
insert into tj_tag values (3, 2);
insert into tj_tag values (3, 3);
insert into tj_tag values (3, 7);
insert into tj_tag values (4, 7);
insert into tj_tag values (5, 7);

insert into t_commande values ('9cdfb439c7876e703e30', NOW() - INTERVAL 1 DAY, null, 'V', 90, 'Bonneau', 'Jean', 'jb@hotmail.fr', '49554a84', 1);
insert into t_commande values ('8dd01288dd803f549e0c', NOW() - INTERVAL 2 DAY, NOW() - INTERVAL 1 DAY, 'P', 69,  'Girofle', 'Claude','cloclo@hotmail.fr', 'b31b4549', 2);
insert into t_commande values ('f25babce5e185934ac69', NOW() - INTERVAL 2 MONTH, NOW() - INTERVAL 1 MONTH, 'D', 130, 'Bonneau', 'Jean', 'jb@hotmail.fr', '49554a84', 1);
insert into t_commande values ('f99f723a1b9f676a500c', NOW() - INTERVAL 3 DAY, NOW() - INTERVAL 1 DAY, 'E', 500, 'Guychard', 'Kevvin', 'xX_KeV_Xx@hotmail.fr', '2c810fee', 1);

insert into tj_contenu values ('9cdfb439c7876e703e30', 3, 1);
insert into tj_contenu values ('9cdfb439c7876e703e30', 7, 1);
insert into tj_contenu values ('9cdfb439c7876e703e30', 10, 2);

insert into tj_contenu values ('8dd01288dd803f549e0c', 13, 1);
insert into tj_contenu values ('8dd01288dd803f549e0c', 17, 1);
insert into tj_contenu values ('8dd01288dd803f549e0c', 19, 1);

insert into tj_contenu values ('f25babce5e185934ac69', 14, 1);
insert into tj_contenu values ('f25babce5e185934ac69', 15, 2);

insert into tj_contenu values ('f99f723a1b9f676a500c', 18, 50);
